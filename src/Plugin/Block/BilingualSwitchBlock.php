<?php

namespace Drupal\bilingual_switch\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Bilingual Language Switcher' block.
 *
 * @Block(
 *   id = "bilingual_switch",
 *   admin_label = @Translation("Bilingual Language Switcher"),
 *   category = @Translation("Multilingual")
 * )
 */
class BilingualSwitchBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Id of the current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $currentLanguage;

  /**
   * List of the available languages.
   *
   * @var \Drupal\Core\Language\LanguageInterface[]
   */
  protected $languages;

  /**
   * Current route name.
   *
   * @var string
   */
  protected $route;

  /**
   * Current route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructs an LanguageBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently active route match object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->currentRouteMatch = $route_match;
    // Get languages, get current route.
    $this->currentLanguage = $this->languageManager->getCurrentLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $urls = $this->languageManager->getLanguageSwitchLinks(LanguageInterface::TYPE_INTERFACE, Url::fromRouteMatch($this->currentRouteMatch));
    $urls = $urls->links ?? [];
    $urls_count = count($urls);
    if ($urls_count != 2) {
      return [];
    }
    // Set cache.
    $items['bilingual_langswitch'] = [
      '#cache' => [
        'contexts' => [
          'languages:language_interface',
          'url',
        ],
      ],
    ];

    $current_language_name = $this->currentLanguage->getName();
    $current_language_id = $this->currentLanguage->getId();

    unset($urls[$current_language_id]);
    $another_language_link_info = reset($urls);
    $another_language_langcode = key($urls);
    $another_language_name = $another_language_link_info['title'];
    /** @var \Drupal\Core\Url $another_language_url */
    $another_language_url = $another_language_link_info['url'];
    if (isset($another_language_link_info['language'])) {
      $another_language = $another_language_link_info['language'];
      $another_language_url->setOption('language', $another_language);
    }
    $another_language_url
      ->setOption('query', $another_language_link_info['query']);

    // Build toolbar item and tray.
    $items['bilingual_langswitch'] += [
      '#type' => 'html_tag',
      '#tag' => 'a',
      'icon' => [
        '#type' => 'html_tag',
        '#tag' => 'i',
        '#value' => $this->t(
          '@prefix @another',
          [
            '@prefix' => $config['bilingual_language_switch_prefix'],
            '@another' => $another_language_name,
          ],
          ['langcode' => $another_language_langcode]
        ),
        '#attributes' => [
          'class' => [
            'fas',
            'fa-language',
            'bilingual-switch-link-icon-' . $another_language_langcode,
          ],
        ],
      ],
      '#attributes' => [
        'class' => [
          'bilingual-switch-link',
        ],
        '#attached' => [
          'library' => [
            'bilingual_switch/bilingual_switch',
          ],
        ],
        'href' => $another_language_url->toString(),
        'title' => $this->t(
          'Switch @current language to @another',
          [
            '@current' => $current_language_name,
            '@another' => $another_language_name,
          ],
          ['langcode' => $another_language_langcode]
        ),
      ],
    ];

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['bilingual_switch_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix Text'),
      '#description' => $this->t('Text to be displayed before the language link.'),
      '#default_value' => $config['bilingual_language_switch_prefix'] ?? $this->t('Switch to'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['bilingual_language_switch_prefix'] = $values['bilingual_switch_prefix'] ?? 'Switch to';
  }

  /**
   * {@inheritdoc}
   *
   * @todo Make cacheable in https://www.drupal.org/node/2232375.
   * Copied from core language block.
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $access = $this->languageManager->isMultilingual() ? AccessResult::allowed() : AccessResult::forbidden();
    return $access->addCacheTags(['config:configurable_language_list']);
  }

}
