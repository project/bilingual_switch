INTRODUCTION
------------

This module adds a block for switching between two languages configured in a
multilingual Drupal site. The block will render a link that will switch between
two different languages. The module will not work if there are a more than two
languages enabled.

REQUIREMENTS
------------

* Requires a multilingual site with 2 languages configured.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.

CONFIGURATION
-------------

This module provides a language switcher as a link in a block. All configuration
is in the
block form. Just place the block in the right place.

MAINTAINERS
-----------

* Cristian García (clgarciab)
